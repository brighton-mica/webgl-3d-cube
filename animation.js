var vertexShaderSource = `#version 300 es
in vec4 a_pos;
in vec4 a_color;

uniform mat4 u_model;
uniform mat4 u_projection;

out vec4 v_color;

void main() {
    gl_Position = u_projection * u_model * a_pos;
    v_color = a_color;
}
`

var fragmentShaderSource = `#version 300 es
precision highp float;

in vec4 v_color;

out vec4 outColor;

void main() {
    outColor = v_color;
}
`



function main() {

    // WebGL
    const canvas = document.getElementById("gl-canvas")
    const gl = canvas.getContext("webgl2")
    if (!gl) {
        console.error("WebGL not supported!")
        return
    }
    

    translate = [gl.canvas.width / 2, gl.canvas.height / 2, 0]
    scale = [1, 1, 1]
    rotate = [0, 0, 0]

    // Vertex Shader
    const vertexShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertexShader, vertexShaderSource)
    gl.compileShader(vertexShader)
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error("Error compiling vertex shader")
        console.log(gl.getShaderInfoLog(vertexShader));
        gl.deleteShader(vertexShader);
        return
    }

    // Fragment Shader
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragmentShader, fragmentShaderSource)
    gl.compileShader(fragmentShader)
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error("Error compiling fragment shader")
        console.log(gl.getShaderInfoLog(fragmentShader));
        gl.deleteShader(fragmentShader);
        return
    }

    // Shader Program
    const shaderProgram = gl.createProgram()
    gl.attachShader(shaderProgram, vertexShader)
    gl.attachShader(shaderProgram, fragmentShader)
    gl.linkProgram(shaderProgram)
    if(!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        console.error("Error linking shader program")
        return
    }

    // Get Attribute/Uniform Locations
    const attributePos = gl.getAttribLocation(shaderProgram, "a_pos")
    const attributeColor = gl.getAttribLocation(shaderProgram, "a_color")
    const modelViewLoc = gl.getUniformLocation(shaderProgram, "u_model")
    const projectionLoc = gl.getUniformLocation(shaderProgram, "u_projection")

    let cubeSize = 125;
    // Geometry
    const vertices = [

       -cubeSize,  cubeSize, -cubeSize, // front
        cubeSize,  cubeSize, -cubeSize,
        cubeSize, -cubeSize, -cubeSize,
       -cubeSize, -cubeSize, -cubeSize, 

       -cubeSize,  cubeSize,  cubeSize, // back
        cubeSize,  cubeSize,  cubeSize,
        cubeSize, -cubeSize,  cubeSize,
       -cubeSize, -cubeSize,  cubeSize, 

        cubeSize,  cubeSize,  cubeSize, // right
        cubeSize,  cubeSize, -cubeSize,
        cubeSize, -cubeSize, -cubeSize,
        cubeSize, -cubeSize,  cubeSize, 
        
       -cubeSize,  cubeSize,  cubeSize, // left
       -cubeSize,  cubeSize, -cubeSize,
       -cubeSize, -cubeSize, -cubeSize,
       -cubeSize, -cubeSize,  cubeSize,
        
       -cubeSize,  cubeSize, -cubeSize, // bottom
        cubeSize,  cubeSize, -cubeSize,
        cubeSize,  cubeSize,  cubeSize,
       -cubeSize,  cubeSize,  cubeSize, 

       -cubeSize, -cubeSize, -cubeSize,
        cubeSize, -cubeSize, -cubeSize,
        cubeSize, -cubeSize,  cubeSize,
       -cubeSize, -cubeSize,  cubeSize, // top
    ]
    const indices = [
        0, 1, 2, // front
        0, 2, 3,

        4, 6, 5, // back
        4, 7, 6,

        8, 10, 9, // right
        8, 11, 10,

        12, 13, 14, // left
        12, 14, 15,

        16, 18, 17, // bottom
        16, 19, 18,

        20, 21, 22, // top
        20, 22, 23,
    ]
    const colors = [
        23, 189, 158, // front
        23, 189, 158,
        23, 189, 158,
        23, 189, 158,

        30, 232, 192, // back
        30, 232, 192,
        30, 232, 192,
        30, 232, 192,

        46, 179, 172, // right 
        46, 179, 172,
        46, 179, 172,
        46, 179, 172,

        30, 214, 205, // left
        30, 214, 205,
        30, 214, 205,
        30, 214, 205,

        0, 173, 118, // bottom
        0, 173, 118,
        0, 173, 118,
        0, 173, 118,

        90, 214, 191, // top
        90, 214, 191,
        90, 214, 191,
        90, 214, 191,
    ]

    const vertexArray = gl.createVertexArray()
    gl.bindVertexArray(vertexArray)

    const positionBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)
    gl.enableVertexAttribArray(attributePos)
    gl.vertexAttribPointer(attributePos, 3, gl.FLOAT, false, 0, 0)

    const colorBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer)
    gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(colors), gl.STATIC_DRAW)
    gl.enableVertexAttribArray(attributeColor)
    gl.vertexAttribPointer(attributeColor, 3, gl.UNSIGNED_BYTE, true, 0, 0)

    const indexBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW)
    gl.bindVertexArray(null)


    let then = 0
    let yRotation = 0
    let xRotation = 0
    
    requestAnimationFrame(DrawScene)

    function DrawScene(now) {
        console.log(yRotation)

        now *= 0.001

        let deltaTime = now - then
        then = now

        yRotation += (-0.7 * deltaTime)
        xRotation += (0.4 * deltaTime)
        yRotation %= 2 * Math.PI
        xRotation %= 2 * Math.PI

        // Clear Canvas
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)
        gl.clearColor(0.0, 0.4, 0.4, 1.0)
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        gl.enable(gl.DEPTH_TEST)
        gl.enable(gl.CULL_FACE)

        gl.useProgram(shaderProgram)
        {
            gl.bindVertexArray(vertexArray)

            let projection = m4.orthographic(0, gl.canvas.width, gl.canvas.height, 0, 400, -400)
            let modelView = m4.identity()
            modelView = m4.rotateX(modelView, xRotation)
            modelView = m4.rotateY(modelView, yRotation)
            modelView = m4.translate(modelView, translate[0], translate[1], translate[2])

            gl.uniformMatrix4fv(modelViewLoc, false, modelView)
            gl.uniformMatrix4fv(projectionLoc, false, projection)
            gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0)

            requestAnimationFrame(DrawScene)
        }
    }
}

main();